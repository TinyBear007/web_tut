from django.shortcuts import render # render template
from django.utils import timezone # get the current time
from datetime import timedelta # get time delta
from .models import *
import random, string

# package for V2
from chatroom.serializers import MsgSerializer, UserSerializer
from rest_framework.response import Response
from rest_framework import status
from rest_framework.decorators import api_view

# Create your views here.
U = 'user_name'
US = 'user_session'
MSG = 'msg'
expire_time = 120

def random_word(length):
   return ''.join(random.choice(string.ascii_lowercase) for i in range(length))

def render_msg(request, user_obj):
    msg_list = Message.objects.filter(record_time__gt=user_obj.msg_update)
    # pack up all the unread messages
    response = render(request, 'chatroom/chatroom_v1.html', {'msg_list':msg_list, U:user_obj.name})
    response.set_cookie(U, user_obj.name, max_age = expire_time)
    response.set_cookie(US, user_obj.session, max_age = expire_time)
    return response

def chatroom_v1(request):
    C = request.COOKIES
    # 2. login request
    if U in request.POST:
        try:
            # 2.1. error when get a non-exist user name
            user_obj = User.objects.get(name = request.POST[U])
            if timezone.now() - timedelta(seconds=expire_time) <= user_obj.session_update:
                response = render(request, 'chatroom/login_v1.html', {"error_msg":"This user already login"})
                response.delete_cookie(U)
                response.delete_cookie(US)
                return response
            user_obj.session = random_word(10)
        except:
            # 2.2. create a user
            user_obj = User(name = request.POST[U], session = random_word(10))
        # return chatroom page
        user_obj.save() #  user_obj need to save before use
        response = render_msg(request, user_obj)
        user_obj.msg_update = timezone.now()
        user_obj.save()
        return response
    # 3. user name and session in cookies
    elif U and US in C:
        user_obj = User.objects.get(name = C[U])
        # 3.1. validate user session
        if user_obj.session == C[US] and timezone.now() - timedelta(seconds=expire_time) <= user_obj.session_update:
            # 3.1.1 store send message if exist
            if MSG in request.POST: # two condition (a) POST request, (b) has field called "msg"
                Message(user = user_obj, text = request.POST[MSG]).save()
            # return chatroom page
            response = render_msg(request, user_obj)
            user_obj.msg_update = timezone.now()
            user_obj.save()
            return response
    # 1. return login page
    response = render(request, 'chatroom/login_v1.html')
    response.delete_cookie(U)
    response.delete_cookie(US)
    return response

def update_cookie(request, user_obj):
    # update cookies expire_time
    response = render(request, 'chatroom/chatroom_v2.html', {U:user_obj.name})
    response.set_cookie(U, user_obj.name, max_age = expire_time)
    response.set_cookie(US, user_obj.session, max_age = expire_time)
    return response

def chatroom_v2(request):
    C = request.COOKIES
    # 2. login request
    if U in request.POST:
        try:
            # 2.1. error when get a non-exist user name
            user_obj = User.objects.get(name = request.POST[U])
            if timezone.now() - timedelta(seconds=expire_time) <= user_obj.session_update:
                response = render(request, 'chatroom/login_v2.html', {"error_msg":"User already logged in"})
                response.delete_cookie(U)
                response.delete_cookie(US)
                return response
            user_obj.session = random_word(10)
        except:
            # 2.2. create a user
            user_obj = User(name = request.POST[U], session = random_word(10))
        user_obj.save()
        return update_cookie(request, user_obj)
    # 3. user name and session in cookies
    elif U and US in C:
        user_obj = User.objects.get(name = C[U])
        # 3.1. validate user session
        if user_obj.session == C[US] and timezone.now() - timedelta(seconds=expire_time) <= user_obj.session_update:
            return update_cookie(request, user_obj)
    # 1. return login page
    response = render(request, 'chatroom/login_v2.html')
    response.delete_cookie(U)
    response.delete_cookie(US)
    return response

@api_view(['GET', 'POST'])
def chatroom_ajax(request): # Rest api for msg
    # Send message
    if U in request.POST:
        user_obj = User.objects.get(name = request.POST[U])
        user_session = request.POST[US]
    # request message update
    elif U in request.GET:
        user_obj = User.objects.get(name = request.GET[U])
        user_session = request.GET[US]
    # validate user name and session
    if user_obj.session == user_session and timezone.now() - timedelta(seconds=expire_time) <= user_obj.session_update:
        # save message
        if MSG in request.data:
            Message(user = user_obj, text = request.data[MSG]).save()
        msg_list = Message.objects.filter(record_time__gt=user_obj.msg_update)
        user_obj.msg_update = timezone.now()
        user_obj.save()
        serializer = MsgSerializer(msg_list, many=True)
        return Response(serializer.data)
    # user name and session combination not valid
    content = {'please move along': 'nothing to see here'}
    return Response(content, status=status.HTTP_404_NOT_FOUND)


@api_view(['GET'])
def user_ajax(request): # Rest api for user
    if U in request.GET:
        print("GET", request.GET)
        try:
            user_obj = User.objects.get(name = request.GET[U])
            if timezone.now() - timedelta(seconds=expire_time) <= user_obj.session_update:
                content = {'login_error': 'User already logged in'}
                return Response(content, status=status.HTTP_403_FORBIDDEN)
            user_obj.session = random_word(10)
        except:
            user_obj = User(name = request.GET[U], session = random_word(10))
        user_obj.save()
        serializer = UserSerializer(user_obj, many=False)
        return Response(serializer.data)
    content = {'please move along': 'nothing to see here'}
    return Response(content, status=status.HTTP_404_NOT_FOUND)
