from rest_framework import serializers
from chatroom.models import Message, User

class MsgSerializer(serializers.ModelSerializer):
    user = serializers.ReadOnlyField(source='user.name')
    class Meta:
        model = Message
        fields = ('user', 'record_time', 'text')

class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ('name', 'session')
