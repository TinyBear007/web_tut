from django.db import models

# Create your models here.

class User(models.Model):
    name = models.CharField(max_length=100)
    session = models.CharField(max_length=10)
    msg_update = models.DateTimeField(auto_now_add = True)
    session_update = models.DateTimeField(auto_now = True)

    def __str__(self):
        return self.name


class Message(models.Model):
    user = models.ForeignKey('User', on_delete = models.CASCADE)
    text = models.TextField()
    record_time = models.DateTimeField(auto_now_add = True)
