function append_msg(data){
	for (i = 0; i < data.length; i++) {
		if (data[i].name == $.cookie('name')) {
			$('#chatroom').append("<p class= 'right_message'>" + "<span style='background-color:#99FF33;'>" + "&nbsp"+ data[i].text + '&nbsp'   + "</span>" + ":(" + data[i].record_time  + ")" + data[i].user + "</p>");
		} else {
			$('#chatroom').append("<p>" + data[i].user + "(" + data[i].record_time + "):" + "<span style='background-color:#99FF33;'>" + "&nbsp" + data[i].text + "&nbsp" + "</span>" + "</p>");
		}


	}
	var date = new Date();
	date.setTime(date.getTime() + (120 * 1000));
	var name = $.cookie('user_name');
	var session = $.cookie('user_session');
	$.cookie('user_name', name, { expires: date });
	$.cookie('user_session', session, { expires: date });
}

$( document ).ready(function() {
	var credentials = {user_name: $.cookie('user_name'), user_session: $.cookie('user_session')};

	$("button").click(function() {
	 	var msg = {"msg": $("textarea").val()};
	  	$.ajax({
		  type: "POST",
		  url: "/msg/?" + "user_name=" + $.cookie('user_name') + "&user_session=" + $.cookie('user_session'),
		  data: JSON.stringify(msg),
		  contentType:"application/json",
		  success: append_msg,
		  error: function () {alert('you are not logged in!');}
		});
	  	$("textarea").val(''); //empty the text area
	});

	setInterval(function(){
	    $.ajax({
	      type:"GET",
	      url:"/msg/",
	      data: credentials,
	      success: append_msg,
	      error: function () {alert('you are not logged in!');}
	    });
	}, 1000);
});
